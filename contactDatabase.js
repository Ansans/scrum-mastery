import { v4 as uuidv4 } from 'uuid';
//database lista objekteille
// let database = [];

// // Testiä varten valmis database
let database = [
    {
      id: '5e7bf10f-1f68-41d9-9f44-ad26541c0089',
      firstName: 'Kalle',
      lastName: 'Viitanen',
      address: 'Kissakuja 4',
      email: '',
      phone: '050 3012301',
      notes: 'kiva kaveri'
    },
    {
      id: '4157653f-8d06-4074-9932-cc443bac9b05',
      firstName: 'Liisa',
      lastName: 'Hermanni',
      address: '',
      email: 'liisa.hermanni@email.fi',
      phone: '',
      notes: 'Kiva viesti'
    }
];

//palauttaa koko objektilistan (ei ole käytössä missään tällä hetkellä)
const getDatabase = () => {
    return database;
};

//Luodaan annettuun objektiin id uuid:llä ja lisätään objektilistaan
const saveData = (contact) => {
    if (contact.firstName !== '' && contact.lastName !== '') {
        if (contact.address !== '' || contact.email !== '' || contact.phone !== '' || contact.notes !== '') {
            //saving contact object in a database
            database = [...database, Object.assign({id:uuidv4()}, contact)];
            console.log('Added a new contact!');
        } else {
            console.log('Adding a contact requires an adress, email, phone number or note.');
        }
    } else {
        console.log('First name and last name are required');
    }
};

const getSortedContacts = () => { 
    return database.map((contact) => {
        return {'id': contact.id, 'name': contact.lastName + ', ' + contact.firstName}
    }).sort((a, b) => a.name.localeCompare(b.name, 'fi'));
 };

const modifyData = (id, updatedContactInfo, web=false) => {
    const contact = database.find(contact => contact.id === id);
    //javascript muokkaa heti kyseistä objektia.
    if (!web) {
        for (let [key,value] of Object.entries(updatedContactInfo)) {
            if (value != '') {
                contact[key] = value;
            }
        }
    }
    //huomasin kommentin siinä Trellossa joten:
    contact['last-modified'] = new Date().toString();
};

//tulostaa objektilistan konsolille
const printDatabase = () => {
    console.log(getDatabase());
};

//Tämä funktio listaa jokaisen kontaktin tiedot käyttäjän näkyville.
const listFunction = () => {
    const tiedot = getDatabase();
    for (let i = 0; i < tiedot.length; i++) {
        let rivi = "";
        rivi += "Etunimi: " + tiedot[i].firstName + "; " + "Sukunimi: " + tiedot[i].lastName;
            
        if (tiedot[i].phone) {
            rivi += "; Puhelinnumero: " + tiedot[i].phone;
        } 
        if (tiedot[i].email) {
            rivi += "; Sähköposti: " + tiedot[i].email;
        }
        if (tiedot[i].address) {
            rivi += "; Katuosoite: " + tiedot[i].address;
        }
        if (tiedot[i].notes) {
            rivi += "; Muut tiedot: " + tiedot[i].notes;
        }
            
        console.log(rivi);
    }
};

//palauttaa database listan ogjektina jossa jokaisen kontaktin key = id ja value = loput tiedot
const getDeleteList = () => {
    return getDatabase().reduce((acc, elem) => ({...acc,[elem.id] : `${elem.firstName} ${elem.lastName}\
, Address: ${elem.address = elem.address || "none"}, E-mail: ${elem.email = elem.email || "none"}\
, Phone: ${elem.phone = elem.phone || "none"}, Notes: ${elem.notes = elem.notes || "none"}`}), {});

};

//deletoi database listasta entryn joka annetaan String parametsina entryToDelete
const deleteData = (entryToDelete) => {
    const object = getDeleteList();
    const idToDelete = Object.keys(object).find(key => object[key] === entryToDelete);
    
    console.log(`Entry ${entryToDelete} deletet`);
    getDatabase().splice(getDatabase().indexOf(getDatabase().find(elem => elem.id === idToDelete)),1);
};

// Search

const searchContact = (search) => {
    const results = [];

    let toSearch = search;
  

   
    for (let i=0; i<database.length; i++) {
        for (let key in database[i]) {
            if (database[i][key].indexOf(toSearch)!=-1) {
                results.push(database[i]);
               
            }
        }
    }
    return results
 };

export default {saveData, getSortedContacts, modifyData, printDatabase, getDatabase, listFunction, getDeleteList, deleteData, searchContact};