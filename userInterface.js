import db from "./contactDatabase.js"; //db eli database ja sen funktiot
import inquirer from 'inquirer'; //inquirer === UI

// main UI:n kysymykset
const questions = {
    'addContact': 'Add contact',
    'modify': 'Modify contact',
    'list': 'List contacts',
    'delete' : 'Delete contact',
    'search': 'Search for contacts',
    'quit': 'Quit'
};

const contactInformation = [
    {name: 'firstName', message: 'First name: '},
    {name: 'lastName', message: 'Last name: '},
    {name: 'address', message: 'Street: '},
    {name: 'email', message: 'Email: '},
    {name: 'phone', message: 'Phone number: '},
    {name: 'notes', message: 'Notes: '}
];

const chooseContact = async (contacts, message) => {
    const answer = await inquirer.prompt([{
        type: 'list',
        name: 'choice',
        message: message,
        choices: contacts.map(contact => contact.name)
    }]);
    return answer.choice;
}

//Esittää deletoitavat tiedot listana josta valita minkä haluaa deletoida ja palauttaa sen Stringinä
const chooseDeleted = async (deleteObject) => {
    const answer = await inquirer.prompt([
        {
            type: 'list',
            name: 'choice',
            message:'Which entry would you like to delete?',
            choices: Object.values(deleteObject),
        },
    ]);
    return answer.choice;
};

//tässä käydään läpi mikä on vastattu ja käytetään yllä importattu db-objekti
const handleChoice = async (answer) => {
    // Add contact
    if (answer.choice === questions.addContact) {
        const contact = await inquirer.prompt(contactInformation);
        db.saveData(contact);

    // Modify contact
    } else if (answer.choice === questions.modify) {
        const allContacts = db.getSortedContacts();
        //laittaa numeron nimen edessa 1)Ankka, Aku 2)Ankka, Aku (hyin lähellä Tomin getDeleteList :))
        if (allContacts.length === 0) {
            console.log('There are no entries to modify');
        } else {
            allContacts.forEach((contact, idx) => contact.name = `${idx+1}) ${contact.name}`);
            const chosenContact = await chooseContact(allContacts, 'Choose contact to modify');
            const id = allContacts.find(contact => chosenContact == contact.name).id;
            const modifiedContact = await inquirer.prompt(contactInformation);
            db.modifyData(id, modifiedContact);
        }
        
    // List contacts
    } else if (answer.choice === questions.list) {
        db.listFunction();
    
    // Delete contact
    } else if (answer.choice === questions.delete) {

        const obj = db.getDeleteList();

        if (obj && Object.keys(obj).length === 0 && Object.getPrototypeOf(obj) === Object.prototype){
            console.log("There are no entires to delete.");
        } else {
            const entryToDelete = await chooseDeleted(obj);
            db.deleteData(entryToDelete);
        }
    }
    // Search contact
    else if (answer.choice === questions.search) {

        const search = await inquirer.prompt([
            {name: 'query', message: 'Search for in contacts: '},
        ]);
        const result = db.searchContact(search.query);
        
      //  console.log(result);
      if (result.length === 0) {
        console.log("No contact found");
    }

    else {
console.log(result); 
}
    } 
    
    // Quit --- jos quit niin lopeta loopia. Return katkaise handleChoicen, eli rivi 54 ei toteudu
    else if (answer.choice === questions.quit) {
        return false;
    }

    return true;
};

//  UI:n main looppi
const run = async () => {
    const choices = Object.values(questions); 
    let loop = true;

    while (loop) {
        const answer = await inquirer.prompt([
            {
                type: 'list',
                name: 'choice',
                message:'Choose one of the following',
                // vähän rumasti tehty: choices an pakollinen avain, mutta olen itse myös nimennyt valuemuuttuujan choicesiksi (rivi 59)
                choices: choices,
            },
        ]);
        //jos true jatka loopia jos false lopeta. loop saa arvon handleChoicesta
        loop = await handleChoice(answer);
    }

    console.log('Exit');
};

//käynnistää looppin
run();
